<?php

use App\Providers\ChargePaymentServicesProvider;
use App\Providers\EventBroadcastersProvider;
use App\Providers\PaymentMethodsProvider;
use App\Services\Payments\Commands\CreatePaymentCommand;
use App\Services\Payments\CreatePaymentService;
use App\Services\Payments\FeeService;
use Money\Money;

require_once './vendor/autoload.php';

$options = getopt('', ['service:','method:','pan:','date:','cvc:', 'number:', 'amount:', 'currency:', 'fee:']);

$argService = $options['service'] ?? 'sberbank';
$argMethod = $options['method'] ?? 'card';
$argPan = $options['pan'] ?? '1234123412341244';
$argDate = new \DateTime($options['date'] ?? '2021-10-15');
$argCvc = $options['cvc'] ?? '123';
$argNumber = $options['number'] ?? '87771352254';
$argAmount = $options['amount'] ?? 1000;
$argCurrency = strtoupper($options['currency'] ?? 'rub');
$argFeePayerAsClient = ($options['fee']??'') == 'client';

$paymentMethodsProvider = new PaymentMethodsProvider();
$paymentMethod = $paymentMethodsProvider->provider($argMethod);

if($argMethod == 'card') {
    $paymentMethod->setPan($argPan);
    $paymentMethod->setExpireDate($argDate);
    $paymentMethod->setCvc($argCvc);
} else if($argMethod == 'number') {
    $paymentMethod->setNumber($argNumber);
}

$createPaymentService = new CreatePaymentService();
$payment = $createPaymentService->handle(new CreatePaymentCommand(Money::$argCurrency($argAmount), $paymentMethod));

$chargePaymentServicesProvider = new ChargePaymentServicesProvider();
$chargePaymentService = $chargePaymentServicesProvider->provider($argService);

$payment->setComission((new FeeService($chargePaymentService, $payment))->calculate());

$payment->setFeePayerAsClient($argFeePayerAsClient);

if(!$chargePaymentService->paymentIsAllowed($payment)) {
    echo 'Error: Payment is not allowed';
    exit(1);
}

$response = $chargePaymentService->createPayment($payment);

if ($response->isCompleted()) {
    echo 'Thank you! Payment completed. Amount: '.$payment->getAmount()->getAmount().', Comission: '.$payment->getCommission()->getAmount().', Net amount: '.$payment->getNetAmount()->getAmount().PHP_EOL;

    if($payment->getAmount()->getCurrency()->getCode() == 'EUR') {
        $broadcaster = (new EventBroadcastersProvider())->provider('telegram');
        $broadcaster->send('Thank you! Payment completed');
    }

} elseif ($response->isFailed()) {
    echo 'Error: Something went wrong! Try another card'.PHP_EOL;
}
echo PHP_EOL;