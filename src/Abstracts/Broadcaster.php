<?php


namespace App\Abstracts;

use App\Contracts\Broadcaster as BroadcasterContract;

class Broadcaster implements BroadcasterContract
{
    public function send(string $message): bool
    {
        return false;
    }
}