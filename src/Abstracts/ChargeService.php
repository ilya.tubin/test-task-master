<?php


namespace App\Abstracts;


use App\Contracts\ChargeService as ChargeServiceContract;
use App\Contracts\Payment as PaymentContract;
use App\Contracts\PaymentResponse;
use Exception;

class ChargeService implements ChargeServiceContract
{
    public function createPayment(PaymentContract $payment): PaymentResponse
    {
        throw new  Exception('method [createPayment] was not implemented');
    }

    public function getAlias(): string
    {
        $target = explode('\\', get_class($this));
        return strtolower(array_pop($target));
    }

    public function paymentIsAllowed(PaymentContract $payment): bool
    {
        return true;
    }
}