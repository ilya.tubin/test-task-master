<?php

namespace App\Abstracts;

use App\Contracts\PaymentMethod as PaymentMethodContract;

abstract class PaymentMethod implements PaymentMethodContract
{
    public function setType(string $type) 
    {
        $this->type = $type;
        return $this;
    }
    
    public function getType(): string
    {
        return $this->type;
    }
}