<?php


namespace App\Broadcasters;

use App\Abstracts\Broadcaster;

class Telegram extends Broadcaster
{
    public function send(string $message): bool
    {
        echo "Telegram message: ".$message.PHP_EOL;

        return true;
    }
}