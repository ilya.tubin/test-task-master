<?php


namespace App\ChargeServices;

use App\Abstracts\ChargeService;
use App\Banks\Responses\Payment;
use App\Contracts\Payment as PaymentContract;
use App\Contracts\PaymentMethod;
use App\Contracts\PaymentResponse;
use Money\Money;

class Sberbank extends ChargeService
{
    public function createPayment(PaymentContract $payment): PaymentResponse
    {
        return new Payment(Payment::STATUS_COMPLETED);
    }
}