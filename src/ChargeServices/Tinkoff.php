<?php


namespace App\ChargeServices;

use App\Abstracts\ChargeService;
use App\Banks\Responses\Payment;
use App\Contracts\Payment as PaymentContract;
use App\Contracts\PaymentMethod;
use App\Contracts\PaymentResponse;
use Exception;
use Money\Money;

class Tinkoff extends ChargeService
{
    public function createPayment(PaymentContract $payment): PaymentResponse
    {
        return new Payment(Payment::STATUS_COMPLETED);
    }

    public function paymentIsAllowed(PaymentContract $payment): bool
    {
        $allowedMethods = ['card'];
        $method = $payment->getPaymentMethod()->getType();

        if(!in_array($method, $allowedMethods)) {
            return false;
        }

        if($payment->getAmount()->getCurrency()->getCode() != 'RUB') {
            return false;
        }

        if($payment->getAmount()->getAmount() < 15000) {
            return false;
        }

        return true;
    }
}