<?php 

return [
    'sberbank' => [
        'card' => [
            'eur' => [
                10000 => [
                    'fee_percent' => 7,
                    'fee_fix' => 1,
                    'fee_min' => 4,
                ]
            ],
            'rub' => [

                10000 => [
                    'fee_percent' => 3,
                    'fee_fix' => 1,
                    'fee_min' => 3,
                ]
            ],
        ],
        'mobile' => [
            'eur' => [],
            'rub' => [],
        ]
    ],
    'qiwi' => [
        'mobile' => [
            'rub' => [
                75 => [
                    'fee_percent' => 5,
                    'fee_fix' => 0,
                    'fee_min' => 3,
                ],
            ],
            'eur' => [

            ]
        ]
    ]
];