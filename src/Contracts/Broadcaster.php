<?php 

namespace App\Contracts;

use App\Contracts\Payment;

interface Broadcaster 
{
    public function send(string $message): bool;
}