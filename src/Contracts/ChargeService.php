<?php 

namespace App\Contracts;

use App\Contracts\Payment;

interface ChargeService 
{
    public function createPayment(Payment $payment): PaymentResponse;
    public function paymentIsAllowed(Payment $payment): bool;
}