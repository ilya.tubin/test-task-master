<?php 

namespace App\Contracts;

interface PaymentMethod 
{
    public function getType(): string;
}