<?php 

namespace App\Contracts;

use DateTime;

interface PaymentMethodCard 
{
    public function getPan(): string;
    public function getExpiryDate(): DateTime;
    public function getCvc(): int;
}