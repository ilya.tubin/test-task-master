<?php 

namespace App\Contracts;

use DateTime;

interface PaymentMethodMobile
{
    public function getNumber(): string;
}