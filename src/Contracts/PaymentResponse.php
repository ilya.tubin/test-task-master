<?php 

namespace App\Contracts;

interface PaymentResponse 
{
    public function isFailed(): bool;
    public function isCompleted(): bool;
}