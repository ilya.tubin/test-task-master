<?php


namespace App\Entities;

use App\Contracts\PaymentMethod;
use App\Contracts\Payment as PaymentContract;
use App\PaymentMethods\Card;
use DateTime;
use Money\Money;

class Payment implements PaymentContract
{

    private Money $amount;
    private Money $commission;
    private PaymentMethod $paymentMethod;
    private DateTime $createdAt;
    private bool $feePayerIsClient;

    public function __construct(Money $amount, Money $commission, PaymentMethod $paymentMethod)
    {
        $this->amount = $amount;
        $this->commission = $commission;
        $this->paymentMethod = $paymentMethod;
        $this->createdAt = new DateTime();
        $this->feePayerIsClient = false;
    }

    public function getAmount(): Money
    {
        return $this->amount;
    }

    public function getCommission(): Money
    {
        return $this->commission;
    }

    public function setComission(Money $commission): Payment 
    {
        $this->commission = $commission;
        return $this;
    }

    public function getPaymentMethod(): PaymentMethod
    {
        return $this->paymentMethod;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function getNetAmount(): Money
    {
        return $this->feePayerIsClient() ? $this->amount->add($this->commission) : $this->amount->subtract($this->commission);
    }

    public function setFeePayerAsClient(bool $value)
    {
        $this->feePayerIsClient = $value;
        return $this;
    }

    public function feePayerIsClient()
    {
        return $this->feePayerIsClient;
    }

}