<?php


namespace App\PaymentMethods;

use App\Abstracts\PaymentMethod;
use App\Contracts\PaymentMethodCard;
use DateTime;

class Card extends PaymentMethod implements PaymentMethodCard
{

    private string $pan;
    private DateTime $expiryDate;
    private int $cvc;
    
    protected string $type;

    public function __construct()
    {
        $this->setType('card');
    }

    public function setPan(string $pan)
    {
        $this->pan = $pan;
        return $this;
    }

    public function getPan(): string
    {
        return $this->pan;
    }

    public function setExpireDate(DateTime $date)
    {
        $this->expiryDate = $date;
        return $this;
    }

    public function getExpiryDate(): DateTime
    {
        return $this->expiryDate;
    }

    public function setCvc(int $cvc)
    {
        $this->cvc = $cvc;
        return $this;
    }

    public function getCvc(): int
    {
        return $this->cvc;
    }
}