<?php


namespace App\PaymentMethods;

use App\Abstracts\PaymentMethod;
use App\Contracts\PaymentMethodMobile;
use DateTime;

class Mobile extends PaymentMethod implements PaymentMethodMobile
{

    private string $number;
    
    protected string $type;

    public function __construct()
    {
        $this->setType('mobile');
    }

    public function setNumber(string $number)
    {
        $this->number = $number;
        return $this;
    }

    public function getNumber(): string
    {
        return $this->number;
    }
}