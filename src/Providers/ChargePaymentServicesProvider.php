<?php 

namespace App\Providers;

use App\Contracts\ChargeService;
use Exception;

class ChargePaymentServicesProvider
{
    private $methods = [];

    public function __construct()
    {
    }

    public function provider(string $provider): ChargeService
    {
        $class = 'App\\ChargeServices\\'.ucfirst($provider);
        $instance = new $class();

        if(!($instance instanceof ChargeService)) {
            throw new Exception('Provider not implemented ChargeService interface');
        }

        return $instance;
    }
}