<?php 

namespace App\Providers;

use App\Contracts\Broadcaster;
use Exception;

class EventBroadcastersProvider
{
    private $methods = [];

    public function __construct()
    {
    }

    public function provider(string $provider): Broadcaster
    {
        $class = 'App\\Broadcasters\\'.ucfirst($provider);
        $instance = new $class();

        if(!($instance instanceof Broadcaster)) {
            throw new Exception('Provider not implemented ChargeService interface');
        }

        return $instance;
    }
}