<?php 

namespace App\Providers;

use App\Contracts\PaymentMethod;
use Exception;

class PaymentMethodsProvider
{
    private $methods = [];

    public function __construct()
    {
    }

    public function provider(string $provider): PaymentMethod
    {
        $class = 'App\\PaymentMethods\\'.ucfirst($provider);
        $instance = new $class();

        if(!($instance instanceof PaymentMethod)) {
            throw new Exception('Provider not implemented PaymentMethod interface');
        }

        return $instance;
    }
}