<?php


namespace App\Services\Payments\Commands;

use App\Contracts\PaymentMethod;
use Money\Money;

class CreatePaymentCommand
{

    private Money $amount;
    private PaymentMethod $paymentMethod;

    public function __construct(Money $amount, PaymentMethod $paymentMethod)
    {
        $this->amount = $amount;
        $this->paymentMethod = $paymentMethod;
    }

    public function getAmount(): Money
    {
        return $this->amount;
    }

    public function getPaymentMethod(): PaymentMethod
    {
        return $this->paymentMethod;
    }
}