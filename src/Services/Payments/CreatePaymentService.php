<?php

namespace App\Services\Payments;

use App\Entities\Payment;
use App\Services\Payments\Commands\CreatePaymentCommand;
use Money\Money;

class CreatePaymentService
{
    public function handle(CreatePaymentCommand $command): Payment
    {
        return new Payment($command->getAmount(), Money::RUB(0), $command->getPaymentMethod());
    }
}