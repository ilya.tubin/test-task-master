<?php

namespace App\Services\Payments;

use App\Contracts\ChargeService;
use App\Contracts\Payment;
use App\Contracts\PaymentMethod;
use App\Support\Config;
use Money\Money;

class FeeService
{   
    private $payment;
    private $chargeService;

    public function __construct(ChargeService $chargeService, Payment $payment)
    {
        $this->payment = $payment;
        $this->chargeService = $chargeService;
    }

    public function calculate(): Money
    {
        $amount = $this->payment->getAmount();
        $service = $this->chargeService->getAlias();
        $method = $this->payment->getPaymentMethod()->getType();
        $currency = $this->payment->getAmount()->getCurrency()->getCode();

        $list = Config::get('fee.'.$service.'.'.$method.'.'.strtolower($currency), []);

        krsort($list);

        $fee = array_values($list)[0] ?? [];

        foreach($list as $stepAmount => $info) {
            if($amount->lessThanOrEqual(Money::$currency($stepAmount))) {
                $fee = $info;
            }
        }

        $fee1 = $amount->multiply($fee['fee_percent']??0)->divide(100)->add(Money::$currency($fee['fee_fix']??0));
        $fee2 = Money::$currency($fee['fee_min']??0);

        return max($fee1, $fee2);
    }
}