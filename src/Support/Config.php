<?php 

namespace App\Support;

use Closure;

class Config {
 
    public static function get($key, $default = null)
    {
        $segments = explode('.', $key);
        $file = array_shift($segments);
        $data = require(dirname(__FILE__).'/../Config/'.ucfirst($file).'.php');
        return self::dataGet($data, implode('.', $segments), $default);
    }

    private static function value($value)
    {
        return $value instanceof Closure ? $value() : $value;
    }

    private static function dataGet($target, $key, $default = null)
    {
        if (is_null($key)) return $target;

        foreach (explode('.', $key) as $segment)
        {
            if (is_array($target))
            {
                if ( ! array_key_exists($segment, $target))
                {
                    return self::value($default);
                }

                $target = $target[$segment];
            }
            elseif (is_object($target))
            {
                if ( ! isset($target->{$segment}))
                {
                    return self::value($default);
                }

                $target = $target->{$segment};
            }
            else
            {
                return self::value($default);
            }
        }

        return $target;
    }

}